using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MusicMaestro : MonoBehaviour
{
	private static MusicMaestro _instance;
	
	public static MusicMaestro instance
	{
		get
		{
			return _instance;
		}
	}
	
	public AudioClip[] music;
	
	public bool isMuted
	{
		get { return audio.mute; }
	}
	
	void Awake()
	{
		if(_instance != null)
		{
			DestroyImmediate(gameObject);
			return;
		}
		_instance = this;
		
		Object.DontDestroyOnLoad(gameObject);
		audio.clip = music[Random.Range(0, music.Length)];
		audio.Play();
		audio.mute = PlayerPrefs.GetInt("MusicMute", 1) == 0;
	}
	
	void OnApplicationQuit()
	{
		_instance = null;
	}
	
	public void Mute()
	{
		audio.mute = true;
		
		PlayerPrefs.SetInt("MusicMute", 1);
		PlayerPrefs.Save();
	}
	
	public void UnMute()
	{
		audio.mute = false;
		PlayerPrefs.SetInt("MusicMute", 0);
		PlayerPrefs.Save();
	}
}

using UnityEngine;

[System.Serializable]
public class SoundEffect
{
	public string name;
	public AudioClip clip;
	public bool loop = false;
	public bool stopOnSceneLoad = false;
	public float volume = 1;
	public float pitch = 1;
	public float fadeIn = 0;
	public float fadeOut = 0;
}
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SFXPlayer : MonoBehaviour
{
	private static SFXPlayer _instance;
	public static SFXPlayer instance
	{
		get { return _instance; }
	}
	
	
	public List<SoundEffect> effectsList;
	public Dictionary<string, SoundEffect> soundEffects;
	
	public bool isMuted
	{
		get { return _isMuted; }
	}
	
	void Awake()
	{
		if(_instance != null)
		{
			if(_instance != this) DestroyImmediate(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);
		
		soundEffects = new Dictionary<string, SoundEffect>();
		foreach (SoundEffect effect in effectsList) {
			soundEffects.Add(effect.name, effect);
		}
		
		activeSources = new List<AudioSource>();
		inactiveSources = new List<AudioSource>();
		
		_isMuted = PlayerPrefs.GetInt("SoundMute", 1) == 0;
	}
	
	void Update()
	{
		AudioSource source;
		for (int i = 0; i < activeSources.Count; i++) {
			source = activeSources[i];
			if(source.isPlaying) continue;
			SetSourceInactive(source);
			i--;
		}
	}
	
	public AudioSource PlayEffect(string clipName)
	{
		if(!soundEffects.ContainsKey(clipName))
		{
			Debug.LogError("Sound Effect does not exist: " + clipName);
			return null;
		}
		
		AudioSource source = GetInactiveSource();
		SoundEffect effect = soundEffects[clipName];
		source.clip = effect.clip;
		source.loop = effect.loop;
		
		if(effect.fadeIn > 0)
			StartCoroutine(FadeVolume(source, 0, effect.volume, effect.fadeIn, false));
		else
			source.volume = effect.volume;
		
		source.pitch = effect.pitch;
		SetSourceActive(source);
		source.Play();
		
		return source;
	}
	
	public void Stop(string clipName)
	{
		AudioSource source = null;
		for (int i = 0; i < activeSources.Count; i++) {
			source = activeSources[i];
			if(source.clip.name == clipName) break;
		}
		
		if(source != null)
		{
			SoundEffect effect = soundEffects[clipName];
			if(effect.fadeOut > 0)
				StartCoroutine(FadeVolume(source, source.volume, 0, effect.fadeOut, true));
			else
				source.Stop();
		}
	}
	
	public void StopAll()
	{
		Debug.Log("Stop All");
		foreach (AudioSource source in activeSources) {
			StartCoroutine(FadeVolume(source, source.volume, 0, 0.2f, true));
		}
	}
	
	public void Mute()
	{
		foreach (AudioSource source in activeSources) {
			source.mute = true;
		}
		
		foreach (AudioSource source in inactiveSources) {
			source.mute = true;
		}
		
		_isMuted = true;
		PlayerPrefs.SetInt("SoundMute", 0);
		PlayerPrefs.Save();
	}
	
	public void UnMute()
	{
		foreach (AudioSource source in activeSources) {
			source.mute = false;
		}
		
		foreach (AudioSource source in inactiveSources) {
			source.mute = false;
		}
		
		_isMuted = false;
		PlayerPrefs.SetInt("SoundMute", 1);
		PlayerPrefs.Save();
	}
	
	void OnLevelWasLoaded()
	{
		foreach (SoundEffect effect in effectsList)
		{
			if(effect.stopOnSceneLoad)
				Stop(effect.name);
		}
	}
	
	private void SetSourceActive(AudioSource source)
	{
		inactiveSources.Remove(source);
		activeSources.Add(source);
		source.gameObject.SetActive(true);
	}
	
	private void SetSourceInactive(AudioSource source)
	{
		activeSources.Remove(source);
		inactiveSources.Add(source);
		source.gameObject.SetActive(false);
	}
	
	private AudioSource GetInactiveSource()
	{
		if(inactiveSources.Count > 0) return inactiveSources[inactiveSources.Count - 1];
		AudioSource source = CreateSource();
		SetSourceInactive(source);
		return source;
	}
	
	private AudioSource CreateSource()
	{
		GameObject go = new GameObject();
		go.SetActive(false);
		go.transform.parent = transform;
		go.AddComponent<AudioSource>();
		go.name = "SFX Audio Source";
		go.audio.mute = SFXPlayer.instance.isMuted;
		DontDestroyOnLoad(go);
		return go.audio;
	}
	
	private IEnumerator FadeVolume(AudioSource source, float start, float end, float time, bool stopOnFinish = false)
	{
		float elapsed = 0;
		while (elapsed < time)
		{
			source.volume = Mathf.Lerp(start, end, elapsed / time);
			elapsed += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		
		if(stopOnFinish) source.Stop();
	}
	
	private bool _isMuted;
	private List<AudioSource> activeSources;
	private List<AudioSource> inactiveSources;
}

using UnityEngine;
using System.Collections;

public class SFXPlanetSelector : MonoBehaviour
{
	public float minVolume = 0.1f;
	public float maxVolume = 0.5f;
	
	public float minDistance = 100;
	public float maxDistance = 1000;
	
	public float damping = 1;
	
	void Update()
	{
		if(_grabSource == null || _selected == null) return;
		
		float distance = Vector3.Distance(transform.position, _selected.position);
		float distPct = 1 - (distance - minDistance) / (maxDistance - minDistance);
		_grabSource.volume = Mathf.Lerp(_grabSource.volume, (maxVolume - minVolume) * distPct + minVolume, Time.deltaTime * damping);
	}
	
	void OnPlanetSelected(GameObject selected)
	{
		_selected = selected.transform;
		_grabSource = SFXPlayer.instance.PlayEffect("grabPlanet");
		SFXPlayer.instance.PlayEffect("selectPlanet");
	}
	
	void OnPlanetDeselected()
	{
		_grabSource = null;
		SFXPlayer.instance.Stop("grabPlanet");
	}
	
	private Transform _selected;
	private AudioSource _grabSource;
}

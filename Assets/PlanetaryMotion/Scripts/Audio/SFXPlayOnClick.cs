﻿using UnityEngine;
using System.Collections;

public class SFXPlayOnClick : MonoBehaviour
{
	public string effectName;
	
	void OnClick()
	{
		SFXPlayer.instance.PlayEffect(effectName);
	}
}

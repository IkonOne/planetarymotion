using UnityEngine;
using System.Collections;

public class VertexColor : MonoBehaviour
{
	public Color newColor = Color.gray;
	
	// Update is called once per frame
	void Update ()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;

		Color[] colors = new Color[mesh.vertices.Length];
		for (int i = 0; i < mesh.vertices.Length; i++) {
			colors[i] = newColor;
		}
		mesh.colors = colors;
		
		Object.Destroy(this);
	}
}

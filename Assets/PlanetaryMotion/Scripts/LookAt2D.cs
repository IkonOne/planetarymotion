using UnityEngine;
using System.Collections;

public class LookAt2D : MonoBehaviour
{
	public GameObject target;
	
	void Update()
	{
		if(target != null)
		{
			float angle = Mathf.Atan2(target.transform.position.y - transform.position.y, target.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler(0, 0, angle);
		}
	}
}

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TextMesh))]
public class Award : MonoBehaviour
{
	public AwardAnnouncer announcer;
	
	public Color color = Color.white;
	public string message = "Award";
	public int fontSize = 128;
	
	void OnEnable()
	{
		_textMesh = GetComponent<TextMesh>();
		string s = "<color=#" + ColorToHex(color) + ">" + message + "</color>";
		_textMesh.fontSize = fontSize;
		_textMesh.text = s;
		transform.localScale = Vector3.one;
		
		announcer.SetActive(gameObject);
		SendMessage("Play");
	}
	
	void OnAwardComplete()
	{
		announcer.SetInactive(gameObject);
	}
	
	private string ColorToHex(Color32 c)
	{
		return c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2") + c.a.ToString("X2");
	}
	
	private TextMesh _textMesh;
}

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class InheritScale : MonoBehaviour
{
	public Transform target;
	
	// Update is called once per frame
	void Update ()
	{
		transform.localScale = target.localScale;
	}
}

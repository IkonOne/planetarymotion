﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(UILabel))]
public class UILableTextFit : MonoBehaviour
{
    public enum FitType
    { 
        First,
        Horizontal,
        Vertical,
        Both
    }
	
	
	public UIPanel panel;
	public UIWidget widget;
    public FitType type = FitType.First;
	public Vector2 relativeLetterSize = Vector2.one;
	public float relativeLineWidth = 1;
	public float maxScale;
	Vector2 m_relativeSize = Vector2.zero;

	// Use this for initialization
	void Start () {
		m_label = GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float width = Screen.width;
		float height = Screen.height;
		
		if(panel != null)
		{
			width = Screen.width;
			height = Screen.height;
		}
		else if(widget != null)
		{
			width = widget.transform.localScale.x;
			height = widget.transform.localScale.y;
		}
		
		m_relativeSize.x = width * relativeLetterSize.x;
		m_relativeSize.y = height * relativeLetterSize.y;
		
		Vector3 local_scale = transform.localScale;
		
		if(type == FitType.First)
		{
			if(m_relativeSize.x > m_relativeSize.y)
				local_scale.x = local_scale.y = width * relativeLetterSize.x;
			else
				local_scale.x = local_scale.y = height * relativeLetterSize.y;
		}
		else
		{
			if(type == FitType.Horizontal)
				local_scale.x = local_scale.y = width * relativeLetterSize.x;
			if(type == FitType.Vertical)
				local_scale.x = local_scale.y = height * relativeLetterSize.y;
			if(type == FitType.Both)
			{
				local_scale.x = width * relativeLetterSize.x;
				local_scale.y = height * relativeLetterSize.y;
			}
		}
		
		transform.localScale = local_scale;
		
		m_label.lineWidth = Mathf.FloorToInt(width * relativeLineWidth);
	}
	
	private UILabel m_label;
}

using UnityEngine;
using System.Collections;

public class UIButtonSubmitScore : MonoBehaviour
{
	public UILabel nameLabel;
	public ScoreDisplay scoreDisplay;
	
	void OnClick()
	{
		HighScores.SubmitLocalScore(Application.loadedLevelName, scoreDisplay.score, nameLabel.text);
	}
}

using UnityEngine;
using System.Collections;

public class LocalHighScoresDisplay : MonoBehaviour
{
	public GameObject entryPrefab;
	public UIPanel panel;
	public UIGridAutoSizer gridSizer;
	public UIWidget background;
	
	public string gameType;
	public int numEntries;
	public int scoreDigits = 6;
	
	void Awake()
	{
		_entries = new GameObject[numEntries];
		GameObject go;
		HighScoreEntry entry;
		for (int i = 0; i < numEntries; i++)
		{
			go = NGUITools.AddChild(gridSizer.gameObject, entryPrefab);
			go.name = go.name.Replace("(Clone)", " " + i.ToString());
			
			entry = go.GetComponent<HighScoreEntry>();
			entry.background.widgetContainer = background;
			entry.SetLayer(gameObject.layer);
			
			_entries[i] = go;
		}
		
		if(string.IsNullOrEmpty(gameType)) gameType = Application.loadedLevelName;
	}
	
	void Update()
	{
		gridSizer.numVerticalCells = numEntries;
	}
	
	void OnUpdateScores()
	{
		for (int i = 0; i < _entries.Length; i++) {
			_entries[i].GetComponent<HighScoreEntry>().SetInfo(i + 1,
				HighScores.GetLocalName(gameType, i),
				HighScores.GetLocalScore(gameType, i));
		}
	}
	
	private GameObject[] _entries;
}

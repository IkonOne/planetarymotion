using UnityEngine;
using System.Collections;

public class EnableButtons : MonoBehaviour
{
	public UIButton[] buttons;
	
	public bool disableOnAwake = true;
	
	void Awake()
	{
		if(disableOnAwake) OnDisableButtons();
	}

	void OnEnableButtons()
	{
		foreach (UIButton button in buttons)
		{
			button.isEnabled = true;
		}
	}
	
	void OnDisableButtons()
	{
		foreach (UIButton button in buttons)
		{
			button.isEnabled = false;
		}
	}
}

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIGrid))]
public class UIGridAutoSizer : MonoBehaviour
{
	public Transform inheritSize;
	public int numVerticalCells = 10;
	
	// Use this for initialization
	void Awake ()
	{
		_grid = GetComponent<UIGrid>();
	}
	
	void Update()
	{
		_grid.cellWidth = inheritSize.localScale.x / _grid.maxPerLine;
		_grid.cellHeight = inheritSize.localScale.y / (numVerticalCells + 1);
		_grid.repositionNow = true;
	}
	
	private UIGrid _grid;
}

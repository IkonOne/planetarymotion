using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class UIButtonCollider : MonoBehaviour
{
	public UIWidget widget;
	
	// Use this for initialization
	void Update () {
		BoxCollider bc = collider as BoxCollider;
		bc.size = new Vector3(widget.transform.localScale.x, widget.transform.localScale.y, 0);
	}
}

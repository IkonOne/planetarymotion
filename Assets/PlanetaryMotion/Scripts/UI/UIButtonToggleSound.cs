using UnityEngine;
using System.Collections;

public class UIButtonToggleSound : MonoBehaviour
{
	public UILabel label;
	
	// Use this for initialization
	void OnEnable ()
	{
		label.text = SFXPlayer.instance.isMuted ? "Sound\nOff" : "Sound\nOn";
	}
	
	// Update is called once per frame
	void OnClick ()
	{
		if(SFXPlayer.instance.isMuted)
		{
			label.text = "Sound\nOn";
			SFXPlayer.instance.UnMute();
		}
		else
		{
			label.text = "Sound\nOff";
			SFXPlayer.instance.Mute();
		}
	}
}

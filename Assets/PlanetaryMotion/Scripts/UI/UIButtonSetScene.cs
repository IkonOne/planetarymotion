using UnityEngine;
using System.Collections;

public class UIButtonSetScene : MonoBehaviour
{
	public UIButtonLoadScene sceneLoader;
	public string scene;
	
	// Use this for initialization
	void OnClick () {
		sceneLoader.scene = scene;
	}
}

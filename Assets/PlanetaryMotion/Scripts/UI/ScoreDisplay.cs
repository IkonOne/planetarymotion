using UnityEngine;
using System.Collections;

public class ScoreDisplay : MonoBehaviour
{
	public UILabel descriptionLabel;
	public UILabel scoreLabel;
	public UISprite background;
	public int score = 0;
	
	void Update()
	{
		scoreLabel.text = score.ToString();
		
		descriptionLabel.depth = scoreLabel.depth = background.depth + 1;
	}
}

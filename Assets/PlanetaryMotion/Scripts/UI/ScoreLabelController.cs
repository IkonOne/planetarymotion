using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreLabelController : MonoBehaviour
{
	public string message = "Score: ";
	public string scoreColor = "FFFFFF";
	public string pointsColor = "00FF00";
	public string multColor = "00FFFF";
	public uint numDigits = 10;
	
	public bool showPoints = true;
	public bool showMultiplier = true;
	
	// Use this for initialization
	void Start () {
		_label = GetComponent<UILabel>();
		_points = new List<uint>();
	}
	
	void FixedUpdate()
	{
		// Score
		string s = GetColor(scoreColor);
		s += "Score: ";
		for (int i = 0; i < numDigits - _score.ToString().Length; i++)
		{
			s += 0;
		}
		s += _score;
		
		// Points
		if(showPoints)
		{
			for (int i = 0; i < _points.Count; i++)
			{
				s += System.Environment.NewLine;
				if(i == 0) s += GetColor(pointsColor);
				s += GetBonus("+", _points[i]);
			}
		}
		
		// Mult
		if(showMultiplier)
		{
			if(_mult != 0)
				s += System.Environment.NewLine + GetColor(multColor) + GetBonus("x", _mult);
		}
		
		_label.text = s;
		
		_points.Clear();
	}
	
	public void SetScore(uint score)
	{
		_score = score;
	}
	
	public void AddPoints(uint points)
	{
		_points.Add(points);
	}
	
	public void SetMult(uint mult)
	{
		_mult = mult;
	}
	
	private string GetColor(string color)
	{
		return "[" + color + "]";
	}
	
	private string GetBonus(string prefix, uint amount)
	{
		return prefix + " " + amount;
	}
	
	private uint _score = 0;
	private List<uint> _points;
	private uint _mult = 1;
	private UILabel _label;
}

﻿using UnityEngine;
using System.Collections;

public class GameOverUI : MonoBehaviour
{
	public UIPanel panel;
	public float fadeTime = 0.5f;
	public iTween.EaseType easeType = iTween.EaseType.easeOutQuad;
	
	void OnEnable()
	{
		iTween.ValueTo(gameObject, iTween.Hash(
			"from", 0,
			"to", 1,
			"time", fadeTime,
			"easeType", easeType,
			"onUpdate", "OnUpdateAlpha"));
	}
	
	void OnUpdateAlpha(float alpha)
	{
		panel.alpha = alpha;
	}
}

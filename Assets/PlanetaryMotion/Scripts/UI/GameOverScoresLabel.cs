using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UILabel))]
public class GameOverScoresLabel : MonoBehaviour
{
	public Color textColor = Color.green;
	
	void Start()
	{
		UILabel label = GetComponent<UILabel>();
		label.text = "[" + Color2Hex(textColor) + "]" + Application.loadedLevelName + "[-] " + label.text;
	}
	
	private string Color2Hex(Color32 color)
	{
		return color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
	}
}

using UnityEngine;
using System.Collections;

public class EnergyBarController : MonoBehaviour
{
	public UIStretch goalBar;
	public UIStretch chaserBar;
	
	public float chaseSpeed = 0.1f;
	
	void Update()
	{
		chaserBar.relativeSize.x = Mathf.MoveTowards(chaserBar.relativeSize.x, goalBar.relativeSize.x, Time.deltaTime * chaseSpeed);
	}
	
	public void SetEnergyPercentage (float percent)
	{
		goalBar.relativeSize.x = Mathf.Clamp(percent, 0.001f, 1);
	}
}

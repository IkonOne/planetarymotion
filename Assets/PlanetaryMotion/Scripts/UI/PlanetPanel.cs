using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIPanel))]
public class PlanetPanel : MonoBehaviour
{
	public UIRoot root;
	public float transitionTime = 0.5f;
	public iTween.EaseType easeType = iTween.EaseType.easeInSine;
	
	// Update is called once per frame
//	void Update ()
//	{
//		percent = Mathf.Clamp(percent, 0, 1);
//		Vector3 target = planetCam.WorldToScreenPoint(planet.position);
//		target.z = 0;
//		transform.position = panelCam.ScreenToWorldPoint(target) * percent;
//	}
	
	void OnTransitionInRight()
	{
		gameObject.SetActive(true);
		iTween.MoveFrom(gameObject, iTween.Hash("x", Screen.width * root.transform.localScale.x,
			"time", transitionTime,
			"easetype", easeType));
	}
	
	void OnTransitionInLeft()
	{
		gameObject.SetActive(true);
		iTween.MoveFrom(gameObject, iTween.Hash("x", -Screen.width * root.transform.localScale.x,
			"time", transitionTime,
			"easetype", easeType));
	}
	
	void OnTransitionOutLeft()
	{
		iTween.MoveTo(gameObject, iTween.Hash("x", -Screen.width * root.transform.localScale.x,
			"time", transitionTime,
			"easetype", easeType,
			"oncomplete", "OnTweenOutComplete"));
	}
	
	void OnTransitionOutRight()
	{
		iTween.MoveTo(gameObject, iTween.Hash("x", Screen.width * root.transform.localScale.x,
			"time", transitionTime,
			"easetype", easeType,
			"oncomplete", "OnTweenOutComplete"));
	}
	
	void OnTweenOutComplete()
	{
		gameObject.SetActive(false);
		transform.position = Vector3.zero;
	}
}

using UnityEngine;
using System.Collections;

public class PauseUI : MonoBehaviour
{
	public PauseManager pauseManager;
	
	public UIPanel panel;
	public AnchorTween mainBackground;
	public AnchorTween soundBackground;
	public iTween.EaseType easeType = iTween.EaseType.easeInOutSine;
	
	public UIEventListener resumeButton;
	
	void Start()
	{
		resumeButton.onClick += OnResumeClicked;
	}
	
	public void EntranceTween()
	{
		panel.alpha = 1;
		mainBackground.OnTranitionIn();
		soundBackground.OnTranitionIn();
	}
	
	public void ExitTween()
	{
		mainBackground.OnTransitionOut();
		soundBackground.OnTransitionOut();
		
		Invoke("OnExitTweenComplete", mainBackground.transitionTime);
	}
	
	void OnExitTweenComplete()
	{
		pauseManager.Resume();
		panel.alpha = 0;
	}
	
	void OnResumeClicked(GameObject go)
	{
		ExitTween();
	}
}

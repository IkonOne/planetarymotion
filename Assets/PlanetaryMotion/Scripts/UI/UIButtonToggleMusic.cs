using UnityEngine;
using System.Collections;

public class UIButtonToggleMusic : MonoBehaviour
{
	public UILabel label;
	
	void OnEnable()
	{
		label.text = MusicMaestro.instance.isMuted ? "Music\nOff" : "Music\nOn";
	}
	
	void OnClick ()
	{
		if(MusicMaestro.instance.isMuted)
		{
			MusicMaestro.instance.UnMute();
			label.text = "Music\nOn";
		}
		else
		{
			MusicMaestro.instance.Mute();
			label.text = "Music\nOff";
		}
	}
}

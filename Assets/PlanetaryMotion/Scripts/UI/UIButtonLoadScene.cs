using UnityEngine;
using System.Collections;

public class UIButtonLoadScene : MonoBehaviour
{
	public string scene;
	public bool reloadCurrent = false;
	
	void OnClick()
	{
		if(string.IsNullOrEmpty(scene))
		{
			if(!reloadCurrent) return;
			scene = Application.loadedLevelName;
		}
		
		Application.LoadLevel(scene);
	}
}

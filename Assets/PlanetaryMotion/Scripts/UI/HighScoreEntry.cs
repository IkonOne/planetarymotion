using UnityEngine;
using System.Collections;

public class HighScoreEntry : MonoBehaviour
{
	public UIStretch background;
	public UILabel entryLabel;
	public UILabel nameLabel;
	public UILabel scoreLabel;
	
	void Awake()
	{
		entryLabel.depth = nameLabel.depth = scoreLabel.depth = background.gameObject.GetComponent<UISprite>().depth + 1;
	}
	
	public void SetInfo(int entry, string name, int score)
	{
		entryLabel.text = entry.ToString() + ".";
		nameLabel.text = name;
		scoreLabel.text = score.ToString();
	}
	
	public void ConfigureBackground(float relativeX, float relativeY)
	{
		background.relativeSize.Set(relativeX, relativeY);
	}
	
	public void SetLayer(int layer)
	{
		background.gameObject.layer = layer;
		entryLabel.gameObject.layer = layer;
		nameLabel.gameObject.layer = layer;
		scoreLabel.gameObject.layer = layer;
	}
}

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIAnchor))]
public class AnchorTween : MonoBehaviour
{
	public enum TransitionOnAwake { None, In, Out };
	
	public TransitionOnAwake transitionOnAwake = TransitionOnAwake.None;
	public float transitionTime = 0.5f;
	public float delayTime = 0;
	
	public iTween.EaseType easeInType = iTween.EaseType.easeOutSine;
	public iTween.EaseType easeOutType = iTween.EaseType.easeInSine;
	
	public Vector2 tweenOffset = Vector2.up;
	
	void Awake()
	{
		iTween.Init(gameObject);
		_anchor = GetComponent<UIAnchor>();
	}
	
	public void OnTranitionIn()
	{
		StartTween(tweenOffset, Vector2.zero, iTween.EaseType.easeOutSine, delayTime);
		_anchor.relativeOffset = tweenOffset;
	}
	
	public void OnTransitionOut()
	{
		StartTween(_anchor.relativeOffset, tweenOffset, iTween.EaseType.easeInSine, delayTime);
	}
	
	void OnYUpdated(float yValue)
	{
		_anchor.relativeOffset.y = yValue;
	}
	
	void OnXUpdated(float xValue)
	{
		_anchor.relativeOffset.x = xValue;
	}
	
	void StartTween(Vector2 vFrom, Vector2 vTo, iTween.EaseType easeType, float delay = 0)
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", vFrom.x,
			"to", vTo.x,
			"easetype", easeType,
			"delay", delay,
			"time", transitionTime,
			"onupdate", "OnXUpdated"));
		iTween.ValueTo(gameObject, iTween.Hash("from", vFrom.y,
			"to", vTo.y,
			"easetype", easeType,
			"delay", delay,
			"time", transitionTime,
			"onupdate", "OnYUpdated"));
	}
	
	private UIAnchor _anchor;
}

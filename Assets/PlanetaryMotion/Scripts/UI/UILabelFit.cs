using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(UILabel))]
public class UILabelFit : MonoBehaviour 
{
    public enum FitType
    { 
        First,
        Horizontal,
        Vertical,
        Both
    }
	
	public UIPanel panel;
	public UIWidget widget;
    public FitType type = FitType.First;
    public float width = 100;
    public float height = 100;
	public Vector2 relativeBounds = Vector2.one;
	public float maxScale;
	Vector2 m_realBounds = new Vector2();
    UILabel m_label = null;

    void OnEnable() 
    {
        m_label = GetComponent<UILabel>();
    }
    
    public void Update() 
    {
		relativeBounds.x = Mathf.Clamp(relativeBounds.x, float.MinValue, float.MaxValue);
		relativeBounds.y = Mathf.Clamp(relativeBounds.y, float.MinValue, float.MaxValue);
		
		width = Screen.width;
		height = Screen.height;
		
		if(panel != null)
		{
			width = Screen.width;
			height = Screen.height;
		}
		else if (widget != null)
		{
			width = widget.transform.localScale.x;
			height = widget.transform.localScale.y;
		}
		
//		width = widget != null ? widget.transform.localScale.x : width;
//		height = widget != null ? widget.transform.localScale.y : height;
		
		m_realBounds.x = width * relativeBounds.x;
		m_realBounds.y = height * relativeBounds.y;
		
        float width_factor = m_realBounds.x / m_label.relativeSize.x;
        float height_factor = m_realBounds.y / m_label.relativeSize.y;

        Vector3 local_scale = m_label.transform.localScale;

        if (type == FitType.First)
        {
            if (width_factor < height_factor) // Find first limiting factor and adjust the other one to achieve uniform scaling
                local_scale.x = local_scale.y = width_factor;
            else
                local_scale.x = local_scale.y = height_factor;
        }
        else 
        {
            if (type == FitType.Both || type == FitType.Horizontal)
                local_scale.x = local_scale.y = width_factor;
            if (type == FitType.Both || type == FitType.Vertical)
                local_scale.y = local_scale.x = height_factor;
        }
		
		if(maxScale > 0)
		{
			local_scale.x = Mathf.Clamp(local_scale.x, 0.01f, maxScale);
			local_scale.y = Mathf.Clamp(local_scale.y, 0.01f, maxScale);
		}

        m_label.transform.localScale = local_scale;
    }

    public void OnDrawGizmos()
    {
        // Handles only the case when pivot is in center. 
        Gizmos.color = Color.cyan;
		Bounds bounds = NGUIMath.CalculateAbsoluteWidgetBounds(transform);
		Vector3 cube = new Vector3(m_realBounds.x * transform.parent.lossyScale.x, m_realBounds.y * transform.parent.lossyScale.y, 0.01f);
        Gizmos.DrawWireCube(bounds.center, cube);
    }
}

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Planet))]
public class PlanetGraphics : MonoBehaviour
{
	public CirclePrimitive circle;
	public float alphaFade = 100;
	
	// Use this for initialization
	void Start ()
	{
//		_lookTarget = GameObject.Find("PlayerPhysics");
		if(_lookTarget == null && name != "Sun") _lookTarget = GameObject.Find("Sun");
	}
	
	public void SetColor(Color color)
	{
		if(circle.color1 == color) return;
		circle.color1 = color;
		color.a = alphaFade;
		circle.color2 = color;
		
		_isDirty = true;
	}
	
	public void SetRadius(float radius)
	{
		if(circle.radius == radius) return;
		circle.radius = radius;
		_isDirty = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(_isDirty)
		{
			circle.RefreshMesh();
			_isDirty = false;
		}
		
		float angle = -10 * Time.deltaTime + transform.eulerAngles.z;
		if(_lookTarget != null)
			angle = Mathf.Atan2(_lookTarget.transform.position.y - transform.position.y, _lookTarget.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0, 0, angle);
	}
	
	private bool _isDirty = false;
	private GameObject _lookTarget;
}

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Rigidbody), typeof(PlanetGraphics))]
public class Planet : MonoBehaviour
{
	public EndlessScore playerScore;
	public PlanetGraphics planetGraphics;
	
	public float Mass = Mathf.PI * 200;
	
	public Color color;
	public float radius;
	
	void Awake()
	{
		transform.localScale = Vector3.one;
		planetGraphics = GetComponent<PlanetGraphics>();
		planetGraphics.SetColor(color);
		planetGraphics.SetRadius(radius);
	}
	
	void Update()
	{
		SetMass(Mass);
		planetGraphics.SetColor(color);
	}
	
	public void SetMass(float mass)
	{
		if(Mass == mass) return;
		Mass = mass;
		radius = Mass / Mathf.PI / 200;
		GetComponent<SphereCollider>().radius = radius;
		planetGraphics.SetRadius(radius);
		rigidbody.mass = mass;
	}
	
	public void AddGravity(GameObject go, float mass)
	{
		Vector3 grav = transform.position - go.transform.position;
		float dist = (go.transform.position - transform.position).sqrMagnitude;
		mass += Mass;
		grav = grav * _G * mass / dist;
		go.rigidbody.AddForce(grav);
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.name == "Player")
		{
			if(playerScore != null) playerScore.ClearMult();
		}
	}
	
	private float _G =  6.67f / 100;
}

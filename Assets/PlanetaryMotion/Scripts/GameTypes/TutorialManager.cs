﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PlayerConstructor), typeof(PlayerCircleBounds))]
public class TutorialManager : MonoBehaviour
{
	public UILabel label;
	public UIPanel panel;
	
	[HideInInspector][SerializeField]public List<string> instructions;
	[HideInInspector][SerializeField]public int enablePlanetSelectorAt = 1;

	// Use this for initialization
	void Start ()
	{
		GetComponent<PlayerConstructor>().player.GetComponent<PlanetSelector>().enabled = false;
	 	if(instructions.Count > 0)
		{
			_currentMessage = 0;
			SetMessage(_currentMessage);
		}
	}
	
	void NextMessage()
	{
		SetMessage(++_currentMessage);
	}
	
	void SetMessage(int number)
	{
		if(number >= instructions.Count)
		{
			iTween.ValueTo(panel.gameObject, iTween.Hash(
				"from", 1.0f,
				"to", 0.0f,
				"time", 0.5f,
				"onupdate", "OnUpdatePanelAlpha",
				"onupdatetarget", gameObject,
				"oncomplete", "OnCompletePanelAlpha",
				"oncompletetarget", gameObject));
			return;
		}
		
		if(enablePlanetSelectorAt == number) GetComponent<PlayerConstructor>().player.GetComponent<PlanetSelector>().enabled = true;
		string s = instructions[number];
		s.Replace("\\n", System.Environment.NewLine);
		label.text = s;
	}
	
	void OnUpdatePanelAlpha(float t)
	{
		panel.alpha = t;
	}
	
	void OnCompletePanelAlpha()
	{
		panel.gameObject.SetActive(false);
		
		GetComponent<PauseManager>().enabled = true;
	}
	
	private int _currentMessage;
}

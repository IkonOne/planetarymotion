using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(EscapeManager))]
public class EscapeSpawner : MonoBehaviour
{
	public GameObject planetPrefab;
	
	public int numPlanets = 10;
	public float minMass = 3000;
	public float maxMass = 15000;

	// Use this for initialization
	void Awake ()
	{
		_escapeManager = GetComponent<EscapeManager>();
		
		_activePlanets = new List<GameObject>();
		_inactivePlanets = new List<GameObject>();
		_vector3 = new Vector3();
		
		_planets = new GameObject[numPlanets];
		for (int i = 0; i < numPlanets; i++) {
			_planets[i] = (GameObject)Instantiate(planetPrefab);
			_planets[i].SetActive(false);
			_planets[i].transform.parent = transform;
		}
		_inactivePlanets.AddRange(_planets);
	}
	
	void Update()
	{
		GameObject[] iter = _activePlanets.ToArray();
		foreach (GameObject go in iter)
		{
			if(go.transform.position.x < _escapeManager.left)
			{
				RemovePlanet(go);
				continue;
			}
		}
	}
	
	public void RemovePlanet(GameObject go)
	{
		if(!_activePlanets.Contains(go)) return;
		
		go.SetActive(false);
		_activePlanets.Remove(go);
		_inactivePlanets.Add(go);
	}
	
	public void SpawnPlanet(float mass, float absoluteHeight)
	{
		if(_inactivePlanets.Count == 0) return;
		
		GameObject go = _inactivePlanets[_inactivePlanets.Count - 1];
		_inactivePlanets.RemoveAt(_inactivePlanets.Count - 1);
		
		Planet planet = go.GetComponent<Planet>();
		planet.SetMass(mass);
		
		_vector3 = Vector3.zero;
		_vector3.x = _escapeManager.right;
		_vector3.y = _escapeManager.dimensions.y * absoluteHeight - _escapeManager.dimensions.y * 0.5f;
		planet.transform.position = _vector3;
		
		go.SetActive(true);
		_activePlanets.Add(go);
	}
	
	private GameObject[] _planets;
	private List<GameObject> _activePlanets;
	private List<GameObject> _inactivePlanets;
	
	private Vector3 _vector3;
	private EscapeManager _escapeManager;
	private Bounds _camBounds;
}

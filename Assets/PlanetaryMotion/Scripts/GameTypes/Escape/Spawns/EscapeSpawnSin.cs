using UnityEngine;
using System.Collections;

public class EscapeSpawnSin : EscapeSpawnEvent
{
	public float distanceStep = 20;
	public float startAngle = 0;
	public float degreeStep = 10;
	public float totalDegrees = 360;
	public float yScale = 0.8f;
	public float mass = 30000;
	
	public override void Restart ()
	{
		_currentAngle = startAngle;
		_currentDistance = randomStart ? Random.Range(0, totalDegrees / degreeStep) : 0;
		
		Spawn ();
	}
	
	void Update()
	{
		_currentDistance += _manager.frameTravelled;
		if(_currentDistance >= distanceStep) Spawn ();
	}
	
	public override void Spawn()
	{
		float ySpawn = Mathf.Sin(_currentAngle * Mathf.Deg2Rad) * 0.5f * yScale;
		ySpawn += 0.5f;
		_spawner.SpawnPlanet(mass, ySpawn);
		
		_currentAngle += degreeStep;
		
		_currentDistance = 0;
	}
	
	public override float GetEventLength ()
	{
		return base.GetEventLength() * (totalDegrees * degreeStep);
	}
	
	private float _currentAngle;
	private float _currentDistance;
}

using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpawnGroup
{
	public Spawn[] spawns;
}

[System.Serializable]
public class Spawn
{
	public float position;
}

public class EscapeSpawnGeneric : EscapeSpawnEvent
{
	public string spawnName = "Generic";
	public bool drawDebug = false;
	public float mass;
	public float spawnInterval = 300;
	public bool spawnImmediately = false;
	
	public SpawnGroup[] spawns;
	
	public override void Restart ()
	{
		_currentDistance = spawnImmediately ? spawnInterval : 0;
		_currentSpawn = randomStart ? Random.Range(0, spawns.Length) : 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_currentDistance += _manager.frameTravelled;
		if(_currentDistance >= spawnInterval) Spawn();
	}
	
	public override void Spawn()
	{
		SpawnGroup g = spawns[_currentSpawn];
		
		foreach (Spawn spawn in g.spawns)
		{
			_spawner.SpawnPlanet(mass, spawn.position);
		}
		
		_currentDistance = 0;
		_currentSpawn++;
		if(_currentSpawn >= spawns.Length) _currentSpawn = 0;
	}
	
	public override float GetEventLength ()
	{
		return base.GetEventLength () * spawns.Length * spawnInterval;
	}
	
	void OnDrawGizmosSelected()
	{
		if(!drawDebug) return;
		EscapeManager manager = GetComponent<EscapeManager>();
		float spacing = spawnInterval * spawns.Length;
		for (int i = 0; i < 3; i++) {
			for (int group = 0; group < spawns.Length; group++) {
				foreach (Spawn spawn in spawns[group].spawns)
				{
					Gizmos.DrawWireSphere(
						new Vector3(spacing * i + spawnInterval * group, manager.dimensions.y * spawn.position - manager.dimensions.y * 0.5f, 0),
						50);
				}
			}
		}
	}
	
	private int _currentSpawn;
	private float _currentDistance;
}

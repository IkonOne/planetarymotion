using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EscapeManager), typeof(PauseManager), typeof(EscapeScore))]
[RequireComponent(typeof(PlayerConstructor))]
public class EscapeGameOver : MonoBehaviour
{
	public GameObject gameOverUI;
	public ScoreDisplay scoreDisplay;
	
	// Use this for initialization
	void Start ()
	{
		_manager = GetComponent<EscapeManager>();
		_pauseManager = GetComponent<PauseManager>();
		_player = GetComponent<PlayerConstructor>().player;
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Plane[] frustrum = GeometryUtility.CalculateFrustumPlanes(_manager.gameCam);
//		if(!GeometryUtility.TestPlanesAABB(frustrum, _manager.player.transform.collider.bounds))
		if(_player.transform.position.x < _manager.left)
		{
			_pauseManager.Pause(false);
			gameOverUI.SetActive(true);
			scoreDisplay.score = GetComponent<EscapeScore>().GetScore();
		}
	}
	
	private EscapeManager _manager;
	private GameObject _player;
	private PauseManager _pauseManager;
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ParticleSystem))]
public class EscapeParticles : MonoBehaviour
{
	public EscapeManager manager;
	public ParticleSystem[] systems;
	public int maxParticles = 200;
	
	void Start()
	{
		_particles = new ParticleSystem.Particle[maxParticles];
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateSystem(particleSystem);
	}
	
	void UpdateSystem(ParticleSystem system)
	{
		int length = system.GetParticles(_particles);
		Vector3 position;
		float left = manager.left;
		float right = manager.right;
		for (int i = 0; i < length; i++) {
			if(_particles[i].position.x < left - _particles[i].size)
			{
				position = _particles[i].position;
				position.x = right + Random.Range(_particles[i].size * 0.5f, _particles[i].size * 3);
				position.y = Random.Range(manager.top, manager.bottom);
				_particles[i].position = position;
			}
			_particles[i].lifetime = 10;
		}
		
		system.SetParticles(_particles, length);
	}
	
	private ParticleSystem.Particle[] _particles;
}

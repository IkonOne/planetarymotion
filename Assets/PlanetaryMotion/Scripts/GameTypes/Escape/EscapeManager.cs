using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(PauseManager), typeof(EscapeGameOver), typeof(PlayerConstructor))]
public class EscapeManager : MonoBehaviour
{
	public Transform gameCam;
	
	public float introSmoothing = 100;
	public float introTime = 5;
	
	public float startSpeed = 10;
	public float maxSpeed = 500;
	public float timeToMax = 60;
	
	public float playerBounceMult = 0.8f;
	public float playerCatchUpStart = 200;
	public float playerCatchUpEnd = 500;
	
	public Vector2 dimensions;
	
	public float currentSpeed
	{
		get { return _currentSpeed; }
	}
	
	public float distanceTravelled
	{
		get { return _distanceTravelled; }
	}
	
	public float frameTravelled
	{
		get { return _frameTravelled; }
	}
	
	public float right
	{
		get { return gameCam.position.x + (dimensions.x * 0.5f); }
	}
	
	public float left
	{
		get { return gameCam.position.x - (dimensions.x * 0.5f); }
	}
	
	public float top
	{
		get { return dimensions.y / 2; }
	}
	
	public float bottom
	{
		get { return -dimensions.y / 2; }
	}
	
	void Start ()
	{
		_currentSpeed = startSpeed;
		_gameTime = 0;
		
		_player = GetComponent<PlayerConstructor>().player;
		_impactParticles = GetComponent<PlayerConstructor>().impactParticles;
	}
	
	void Update ()
	{
		if(!Application.isPlaying) return;
		
		if(_intro)
		{
			_player.rigidbody.velocity = Vector3.Lerp(_player.rigidbody.velocity, Vector3.right * _currentSpeed, introSmoothing * Time.deltaTime);
			introTime -= Time.deltaTime;
			if(introTime <= 0)
			{
				GetComponent<EscapeGameOver>().enabled = true;
				_intro = false;
			}
		}
		
		if(_player.transform.position.y > dimensions.y / 2 || _player.transform.position.y < -dimensions.y / 2)
		{
			_player.rigidbody.velocity = Vector3.Reflect(_player.rigidbody.velocity, Vector3.up) * playerBounceMult;
			_player.transform.position = new Vector3(_player.transform.position.x,
				Mathf.Clamp(_player.transform.position.y, -dimensions.y / 2,
				dimensions.y / 2));
			EmitImpactParticles();
		}
		
		_currentSpeed = Mathf.Lerp(startSpeed, maxSpeed, _gameTime / timeToMax);
		if(_player.transform.position.x > gameCam.position.x + playerCatchUpStart && _player.rigidbody.velocity.x > _currentSpeed)
		{
			float t = (_player.transform.position.x - (gameCam.position.x + playerCatchUpStart)) / (playerCatchUpEnd - playerCatchUpStart);
			float distance = Mathf.Lerp(0, _player.rigidbody.velocity.x, t);
			_currentSpeed += distance;
		}
		_frameTravelled = _currentSpeed * Time.deltaTime;
		_distanceTravelled += _frameTravelled;
		_gameTime += Time.deltaTime;
		
		gameCam.Translate(Vector3.right * _currentSpeed * Time.deltaTime);
	}
	
	void OnDrawGizmos()
	{
		Vector3 position = gameCam.position;
		position.z = 0;
		Gizmos.DrawWireCube(position, dimensions);
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(position + Vector3.right * playerCatchUpStart, Vector3.up * 1000);
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(position + Vector3.right * playerCatchUpEnd, Vector3.up * 1000);
	}
	
	void EmitImpactParticles()
	{
		if(_impactParticles != null)
		{
			_impactParticles.transform.position = _player.transform.position;
			_impactParticles.transform.LookAt(_player.transform.position + (_player.transform.position.y == dimensions.y / 2 ? Vector3.down : Vector3.up));
			_impactParticles.Play();
		}
		
		SFXPlayer.instance.PlayEffect("impact");
	}
	
	private float _currentSpeed;
	private float _gameTime;
	private float _distanceTravelled;
	private float _frameTravelled;
	
	private bool _intro = true;
	private ParticleSystem _impactParticles;
	private GameObject _player;
}

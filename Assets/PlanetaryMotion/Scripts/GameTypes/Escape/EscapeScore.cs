using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EscapeManager))]
public class EscapeScore : MonoBehaviour
{
	public ScoreLabelController scoreLabel;
	
	void Start()
	{
		_manager = GetComponent<EscapeManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		_score += _manager.frameTravelled;
		
		scoreLabel.SetScore((uint)_score);
	}
	
	public int GetScore()
	{
		return (int)_score;
	}
	
	private float _score;
	private EscapeManager _manager;
}

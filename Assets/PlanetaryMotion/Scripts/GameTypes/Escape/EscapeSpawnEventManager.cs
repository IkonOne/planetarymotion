using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(EscapeManager))]
public class EscapeSpawnEventManager : MonoBehaviour
{
	public EscapeSpawnGeneric gapEvent;
	public EscapeSpawnEvent[] events;
	
	public float eventStart = 5;
	public float minEventSpacing = 0;
	public float maxEventSpacing = 5;
	
	// Use this for initialization
	void Start ()
	{
		_usedEvents = new Queue<EscapeSpawnEvent>();
		_manager = GetComponent<EscapeManager>();
		StopEvent();
		
		PauseBehaviour pb = GetComponent<PauseBehaviour>();
		pb.behavioursToPause.Add(gapEvent);
		pb.behavioursToPause.AddRange(events);
	}
	
	void Update()
	{
		if(_startDistance > 0)
		{
			_startDistance -= _manager.frameTravelled;
			if(_startDistance <= 0) StartEvent();
		}
		
		if(_stopDistance > 0)
		{
			_stopDistance -= _manager.frameTravelled;
			if(_stopDistance <= 0) StopEvent();
		}
	}
	
	void StartEvent()
	{
		if(_currentEvent != null)
		{
			_currentEvent.enabled = false;
		}
		
		_currentEvent = PickEvent();
		_currentEvent.enabled = true;
		_currentEvent.Restart();
		_stopDistance = _currentEvent.GetEventLength();
		
		gapEvent.enabled = false;
		_startDistance = 0;
	}
	
	void StopEvent()
	{
		if(_currentEvent != null) _currentEvent.enabled = false;
		
		_startDistance = gapEvent.GetEventLength();
		
		gapEvent.enabled = true;
		gapEvent.Restart();
	}
	
	private EscapeSpawnEvent PickEvent()
	{
		EscapeSpawnEvent e;
		do {
			e = events[Random.Range(0, events.Length)];
		} while (_usedEvents.Contains(e));
		
		_usedEvents.Enqueue(e);
		if(_usedEvents.Count >= events.Length) _usedEvents.Dequeue();
		return e;
	}
	
	private EscapeManager _manager;
	private EscapeSpawnEvent _currentEvent;
	
	private float _startDistance;
	private float _stopDistance;
	
	private Queue<EscapeSpawnEvent> _usedEvents;
}
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EscapeSpawner), typeof(EscapeManager))]
public class EscapeSpawnEvent : MonoBehaviour
{
	public int minLoops = 1;
	public int maxLoops = 2;
	
	public bool randomStart = false;
	
	// Use this for initialization
	public virtual void Awake ()
	{
		_manager = GetComponent<EscapeManager>();
		_spawner = GetComponent<EscapeSpawner>();
	}
	
	public virtual void Restart()
	{
		
	}
	
	public virtual void Spawn()
	{
		
	}
	
	public virtual float GetEventLength()
	{
		return Random.Range(minLoops, maxLoops);
	}
	
	protected EscapeSpawner _spawner;
	protected EscapeManager _manager;
}

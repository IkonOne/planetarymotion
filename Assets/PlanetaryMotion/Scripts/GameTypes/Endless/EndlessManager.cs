using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PauseManager), typeof(PlayerConstructor))]
public class EndlessManager : MonoBehaviour
{
	public GameObject gameOverUI;
	public GameObject player;
	public MidPoint cameraTarget;
	
	void Awake()
	{
		player = GetComponent<PlayerConstructor>().player;
		player.GetComponent<MessageBroadcaster>().targets.Add(gameObject);
		cameraTarget.target1 = player.transform;
		_pauseManager = GetComponent<PauseManager>();
		
		GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");
		for (int i = 0; i < planets.Length; i++) {
			if(planets[i].name == "Sun") continue;
			_pauseManager.toPause.Add(planets[i].GetComponent<PauseBehaviour>());
		}
	}
	
	void OnEnergyDepleated()
	{
		_pauseManager.Pause(false);
		gameOverUI.SetActive(true);
		gameOverUI.GetComponentInChildren<ScoreDisplay>().score = GetComponent<EndlessScore>().GetScore();
	}
	
	private PauseManager _pauseManager;
}

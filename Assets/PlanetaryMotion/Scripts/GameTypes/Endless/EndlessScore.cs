using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EndlessManager))]
public class EndlessScore : MonoBehaviour
{
	public ScoreLabelController labelController;
	public GameObject player;
	
	public int loopScore = 10;
	public float velocityScoreMult = 0.1f;
	
	// Use this for initialization
	void Start ()
	{
		player = GetComponent<EndlessManager>().player;
		
		_score = 0;
		_mult = 1;
	}
	
	void FixedUpdate()
	{
		AddScore((uint)(player.rigidbody.velocity.magnitude * velocityScoreMult));
	}
	
	public int GetScore()
	{
		return (int)_score;
	}
	
	public void AddBonus(uint bonus)
	{
		if(bonus == 0) return;
		_score = (uint)Mathf.Clamp(_score + bonus * _mult, 0, float.MaxValue);
	}
	
	public void AddScore(uint score)
	{
		if(score == 0) return;
		AddBonus(score);
		labelController.SetScore((uint)Mathf.FloorToInt(_score));
		labelController.AddPoints((uint)Mathf.RoundToInt(score));
	}
	
	public void AddMult(uint mult)
	{
		_mult += mult;
		labelController.SetMult(_mult);
	}
	
	public void ClearMult()
	{
		_mult = 1;
		labelController.SetMult(1);
	}
	
	void OnPlayerLooped(GameObject looped)
	{
		AddBonus((uint)loopScore);
		AddMult(1);
		AwardAnnouncer.instance.Announce(looped.transform.position, "x" + _mult, ColorScheme.instance.MultiplierColor);
		
	}
	
	void OnPlanetCollisionEnter(Collision col)
	{
		if(_mult == 1) return;
		ClearMult();
		
		if(_lastPlanetCollided != col.gameObject) AwardAnnouncer.instance.Announce(col.gameObject.transform.position, "x1", ColorScheme.instance.MultiplierColor);
		_lastPlanetCollided = col.gameObject;
	}
	
	private uint _score;
	private uint _mult;
	private GameObject _lastPlanetCollided;
}

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraSortMode : MonoBehaviour
{
	public TransparencySortMode sortMode = TransparencySortMode.Default;

	// Use this for initialization
	void Start ()
	{
		camera.transparencySortMode = sortMode;
	}
}

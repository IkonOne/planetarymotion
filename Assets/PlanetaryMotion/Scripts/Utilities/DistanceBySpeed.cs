using UnityEngine;
using System.Collections;

public class DistanceBySpeed : MonoBehaviour
{
	public Rigidbody rb;
	
	public Vector3 axis = Vector3.up;
	
	public float maxSpeed = 200;
	public float minDist = 1;
	public float maxDist = 2;
	
	// Update is called once per frame
	void Update ()
	{
		float speed = rb.velocity.magnitude;
		float dist = (speed / maxSpeed) * (maxDist - minDist) + minDist;
		transform.localPosition = axis * dist;
	}
}

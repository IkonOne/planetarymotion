using UnityEngine;
using System.Collections;

public class FollowPlayerOrtho : MonoBehaviour
{
	public Transform target;
	public float strength = 2;
	
	public Transform zoomTarget;
	public float minZoom = 40;
	public float zoomScale = 0.6f;
	public float zoomSpeed = 1;
	
	void Start()
	{
		zoomTarget = GameObject.Find("Sun").transform;
	}
	
	void Update ()
	{
		if(target == null) return;
		
		Vector2 targetPos = new Vector2(target.position.x, target.position.y);
		Vector2 camPos = new Vector2(transform.position.x, transform.position.y);
		
		camPos = Vector2.Lerp(camPos, targetPos, Time.deltaTime * strength);
		
		transform.position = new Vector3(camPos.x, camPos.y, transform.position.z);
		
		if(zoomTarget != null) camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, minZoom + Vector3.Distance(transform.position, zoomTarget.position) * zoomScale, Time.deltaTime * zoomSpeed);
	}
}

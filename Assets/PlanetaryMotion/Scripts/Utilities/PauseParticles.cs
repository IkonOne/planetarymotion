using UnityEngine;
using System.Collections;

public class PauseParticles : MonoBehaviour {

	// Use this for initialization
	void Start () {
		particleSystem.Pause();
	}
}

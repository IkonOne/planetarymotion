using UnityEngine;
using System.Collections;

public class LookAtVelocity : MonoBehaviour
{
	public GameObject velocityGO;
	public Vector3 additionalVelocity = Vector3.zero;
	public float smoothing = 5;
	
	void Update()
	{
		Vector3 euler = transform.eulerAngles;
		euler.z = Mathf.Atan2(velocityGO.rigidbody.velocity.y + additionalVelocity.y, velocityGO.rigidbody.velocity.x + additionalVelocity.x) * Mathf.Rad2Deg;
		euler.z -= 90;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(euler), Time.deltaTime * smoothing);
		
		additionalVelocity = Vector3.zero;
	}
}

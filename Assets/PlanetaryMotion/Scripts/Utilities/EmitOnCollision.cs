using UnityEngine;
using System.Collections;

public class EmitOnCollision : MonoBehaviour
{
	public ParticleSystem particles;
	
	public float minForce = 70;

	// Use this for initialization
	void OnCollisionEnter(Collision col)
	{
		if(col.relativeVelocity.magnitude < minForce) return;
		
		particles.transform.position = col.contacts[0].point;
		particles.transform.LookAt(particles.transform.position + col.contacts[0].normal);
		particles.Play();
	}
}

using UnityEngine;
using System.Collections;

public class HighScores
{
	public static string GetLocalName(string gameType, int position)
	{
		return PlayerPrefs.GetString(NameString(gameType, position), "...");
	}
	
	public static int GetLocalScore(string gameType, int position)
	{
		return PlayerPrefs.GetInt(ScoreString(gameType, position), 0);
	}
	
	public static bool SubmitLocalScore(string gameType, int score, string name)
	{
		int position = -1;
		for (int i = 0; i < 10; i++)
		{
			if(score >= PlayerPrefs.GetInt(ScoreString(gameType, i)))
			{
				position = i;
				break;
			}
		}
		if(position == -1) return false;  // No new high score.
		
		for (int i = 9; i > position; i--)
		{
			if(i == 0) break;
			PlayerPrefs.SetInt(ScoreString(gameType, i), PlayerPrefs.GetInt(ScoreString(gameType, i - 1), 0));
			PlayerPrefs.SetString(NameString(gameType, i), PlayerPrefs.GetString(NameString(gameType, i - 1), "..."));
		}
		PlayerPrefs.SetInt(ScoreString(gameType, position), score);
		PlayerPrefs.SetString(NameString(gameType, position), name);
		
		PlayerPrefs.Save();
		
		return true;
	}
				
	private static string ScoreString(string gameType, int position)
	{
		return gameType + "." + position.ToString() + ".Score";
	}
	
	private static string NameString(string gameType, int position)
	{
		return gameType + "." + position.ToString() + ".Name";
	}
}

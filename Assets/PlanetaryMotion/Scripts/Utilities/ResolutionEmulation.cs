using UnityEngine;
using System.Collections;

public class ResolutionEmulation : MonoBehaviour
{
	
	public Vector2 resolution;

	// Use this for initialization
	void Start ()
	{
		Resolution currRes = Screen.currentResolution;
		
		camera.rect = new Rect((1 - resolution.x / currRes.width) / 2, (1 - resolution.y / currRes.height) / 2, resolution.x / currRes.width, resolution.y / currRes.height);
	}
}

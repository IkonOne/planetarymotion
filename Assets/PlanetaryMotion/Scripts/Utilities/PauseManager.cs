using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PauseManager : MonoBehaviour
{
	public List<PauseBehaviour> toPause;
	public PauseUI pauseUI;
	
	public bool isPaused = false;
	
	void Start()
	{
		pauseUI.pauseManager = this;
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape) && !isPaused) Pause();
	}
	
	public void Pause(bool showUI = true)
	{
		isPaused = true;
		
		foreach (PauseBehaviour behaviour in toPause) {
			behaviour.Pause();
		}
		
		if(pauseUI != null && showUI)
		{
			pauseUI.gameObject.SetActive(true);
			pauseUI.EntranceTween();
		}
	}
	
	public void Resume()
	{
		isPaused = false;
		
		foreach (PauseBehaviour behaviour in toPause) {
			behaviour.Resume();
		}
		
		if(pauseUI != null) pauseUI.gameObject.SetActive(false);
	}
}

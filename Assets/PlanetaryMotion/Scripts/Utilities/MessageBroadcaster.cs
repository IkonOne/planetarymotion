using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MessageBroadcaster : MonoBehaviour
{
	public List<GameObject> targets;
	
	public void Broadcast(string methodName, object parameter = null, SendMessageOptions options = SendMessageOptions.RequireReceiver)
	{
		BroadcastMessage(methodName, parameter, options);
		foreach (GameObject go in targets) {
			go.BroadcastMessage(methodName, parameter, options);
		}
	}
}

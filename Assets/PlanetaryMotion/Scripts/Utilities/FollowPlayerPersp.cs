using UnityEngine;
using System.Collections;

public class FollowPlayerPersp : MonoBehaviour
{

	public Transform target;
	public float strength = 2;
	
	public Transform zoomTarget;
	public float minDist = 0;
	public float maxDist = 500;
	public float baseZ = -500;
	public float minZ = 0;
	public float maxZ = 500;
	public float zoomScale = 0.6f;
	public float zoomSpeed = 1;
	
	void Start()
	{
		zoomTarget = GameObject.Find("Sun").transform;
	}
	
	void Update ()
	{
		if(target == null) return;
		
		Vector2 targetPos = new Vector2(target.position.x, target.position.y);
		Vector2 camPos = new Vector2(transform.position.x, transform.position.y);
		
		camPos = Vector2.Lerp(camPos, targetPos, Time.deltaTime * strength);
		float targetZ = scale(Vector3.Distance(target.position, zoomTarget.position), minDist, maxDist, minZ, maxZ);
		targetZ = Mathf.Lerp(transform.position.z, baseZ - targetZ, Time.deltaTime * strength);
		transform.position = new Vector3(camPos.x, camPos.y, targetZ);
		
//		if(zoomTarget != null) camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, minZoom + Vector3.Distance(transform.position, zoomTarget.position) * zoomScale, Time.deltaTime * zoomSpeed);
		
	}
			
	float scale(float scale, float min1, float max1, float min2, float max2)
	{
		return min2 + ((scale - min1) / (max1 - min1)) * (max2 - min2);
	}
}

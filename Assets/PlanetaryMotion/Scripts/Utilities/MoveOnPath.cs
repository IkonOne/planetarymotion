using UnityEngine;
using System.Collections;

public class MoveOnPath : MonoBehaviour
{
	public Transform[] path;
	public Transform pathParent;
	
	public float speed = 0.1f;
	public float parentRotSpeed = 2;
	
	void Start()
	{
		_t = 0;
		UpdatePosition();
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		_t += Time.deltaTime * speed;
		if(_t > 1) _t = 0;
		
		UpdatePosition();
	}
	
	private void UpdatePosition()
	{
		transform.position = iTween.PointOnPath(path, _t);
	}
	
	private float _t;
}

using UnityEngine;
using System.Collections;

public class PunchCameraOnCollision : MonoBehaviour
{
	public string collisionTag = "Planet";
	public Vector2 shakeAmount;
	public float minforce = 30;
	public float shakeTime = 0.5f;
	
	void OnCollisionEnter(Collision col)
	{
		if(!string.IsNullOrEmpty(collisionTag) && !col.gameObject.CompareTag(collisionTag)) return;
		if(Camera.main.GetComponent<iTween>() != null && Camera.main.GetComponent<iTween>().enabled) return;
		if(col.relativeVelocity.magnitude < minforce) return;
		
		_shakeVec = col.relativeVelocity.normalized * (col.relativeVelocity.magnitude - minforce);
		_shakeVec.x *= shakeAmount.y;
		_shakeVec.y *= -shakeAmount.x;
		_shakeVec.z = 0;
		
		iTween.PunchRotation(Camera.main.gameObject, iTween.Hash("amount", _shakeVec, "time", shakeTime, "oncomplete", "OnPunchComplete", "oncompletetarget", gameObject));
	}
	
	void OnPunchComplete()
	{
		Camera.main.transform.rotation = Quaternion.identity;
	}
	
	private Vector3 _shakeVec = new Vector3();
}

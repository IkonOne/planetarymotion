using UnityEngine;
using System.Collections;

public class MidPoint : MonoBehaviour
{
	public Transform target1;
	public Transform target2;
	
	public string name1;
	public string name2;
	
	void Start()
	{
		if(target1 == null) target1 = GameObject.Find(name1).transform;
		if(target2 == null) target2 = GameObject.Find(name2).transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.position = ((target2.position - target1.position) / 2) + target1.position;
	}
}

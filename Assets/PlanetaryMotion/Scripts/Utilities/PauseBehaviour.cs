using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PauseBehaviour : MonoBehaviour
{
	public List<MonoBehaviour> behavioursToPause;
	public List<Renderer> renderersToPause;
	public bool pauseRigidbody = true;
	
	public void Pause()
	{
		SendMessage("OnPause", SendMessageOptions.DontRequireReceiver);
		_pausedBehaviours.Clear();
		foreach (MonoBehaviour behaviour in behavioursToPause) {
			if(behaviour.enabled) _pausedBehaviours.Add(behaviour);
			behaviour.enabled = false;
		}
		
		foreach (Renderer rend in renderersToPause) {
			rend.enabled = false;
		}
		
		if(pauseRigidbody && rigidbody != null)
		{
			_savedVelocity = rigidbody.velocity;
			_savedAngularVelocity = rigidbody.angularVelocity;
			rigidbody.isKinematic = true;
		}
	}
	
	public void Resume()
	{
		foreach (MonoBehaviour behaviour in _pausedBehaviours) {
			behaviour.enabled = true;
		}
		
		foreach(Renderer rend in renderersToPause) {
			rend.enabled = true;
		}
		
		if(pauseRigidbody && rigidbody != null)
		{
			rigidbody.isKinematic = false;
			rigidbody.AddForce(_savedVelocity, ForceMode.VelocityChange);
			rigidbody.AddTorque(_savedAngularVelocity, ForceMode.VelocityChange);
		}
		SendMessage("OnResume", SendMessageOptions.DontRequireReceiver);
	}
	
	private Vector3 _savedVelocity;
	private Vector3 _savedAngularVelocity;
	private List<MonoBehaviour> _pausedBehaviours = new List<MonoBehaviour>();
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class CirclePrimitive : MonoBehaviour
{
	// The number of points per quadrant
	public int numPoints = 10;
	
	public float radius = 5;
	public float antialias = 1;
	
	public Color color1 = Color.red;
	public Color color2 = Color.blue;
	
	void Awake()
	{
		_meshFilter = GetComponent<MeshFilter>();
		_meshFilter.mesh = new Mesh();
		
		RefreshMesh();
	}
	
	void Update()
	{
		if(Application.isEditor && !Application.isPlaying) RefreshMesh();
	}
	
	public void RefreshMesh()
	{
		// Verticies
		_verticies.Clear();
		_colors.Clear();
		_uv.Clear();
		_triangles.Clear();
		
		int numVerticies = numPoints * 4;
		
		float angleStep = 2 * Mathf.PI / numVerticies;
		Vector2 uv = new Vector2();
		Vector2 pos = new Vector2();
		for (int i = 0; i < numVerticies / 2; i++)
		{
			pos.x = Mathf.Cos(angleStep * i);
			pos.y = Mathf.Sin(angleStep * i);
			uv.x = (-pos.x + 1) / 2;
			uv.y = (pos.y + 1) / 2;
			
			_verticies.Add(pos * radius);
			_uv.Add(uv);
			_colors.Add(Color.Lerp(color1, color2, (-pos.x + 1) / 2));
			
			if(i > 0 && i < numVerticies / 2)
			{
				pos.y = -pos.y;
				_verticies.Add(pos * radius);
				uv.y = 1 - uv.y;
				_uv.Add(uv);
				_colors.Add(_colors[_colors.Count - 1]);
			}
		}
		pos.Set(-radius, 0);
		uv.Set(0, 1);
		_verticies.Add(pos);
		_uv.Add(uv);
		_colors.Add(color2);
		
		// Triangles 
		for (int i = 0; i < numVerticies - 2; i += 2)
		{
			_triangles.Add(i + 2);
			_triangles.Add(i + 1);
			_triangles.Add(i + 0);
			
			_triangles.Add(i + 1);
			_triangles.Add(i + 2);
			_triangles.Add(i + 3);
		}
		
		// Mesh
		Mesh mesh = _meshFilter.sharedMesh;
		mesh.Clear();
		mesh.vertices = _verticies.ToArray();
		mesh.uv = _uv.ToArray();
		mesh.triangles = _triangles.ToArray();
		mesh.colors = _colors.ToArray();
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		_meshFilter.mesh = mesh;
	}
	
	private List<Vector3> _verticies = new List<Vector3>();
	private List<Vector2> _uv = new List<Vector2>();
	private List<int> _triangles = new List<int>();
	private List<Color> _colors = new List<Color>();
	
	private MeshFilter _meshFilter;
}

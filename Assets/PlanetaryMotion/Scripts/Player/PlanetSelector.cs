using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MessageBroadcaster), typeof(SFXPlanetSelector))]
public class PlanetSelector : MonoBehaviour
{
	public static GameObject selected;
	public float hitSize = 50;
	public float touchSize = 0.5f;
	
	void Awake()
	{
		_broadcaster = GetComponent<MessageBroadcaster>();
	}
	
	void Update ()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Select(Input.mousePosition);
		}
		if(Input.GetMouseButtonUp(0))
		{
			Deselect();
		}
	}
	
	private void Select(Vector2 pos)
	{
		RaycastHit hit = new RaycastHit();
		
		if(Physics.SphereCast(Camera.main.ScreenPointToRay(pos), hitSize, out hit))
		{
			string layerName = LayerMask.LayerToName(hit.collider.gameObject.layer);
			if(layerName == "Planets")
			{
				selected = hit.collider.gameObject;
				_broadcaster.Broadcast("OnPlanetSelected", selected, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	
	private void Deselect()
	{
		_broadcaster.Broadcast("OnPlanetDeselected", selected, SendMessageOptions.DontRequireReceiver);
		selected = null;
	}
	
	private MessageBroadcaster _broadcaster;
}

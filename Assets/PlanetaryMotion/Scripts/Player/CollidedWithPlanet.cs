using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MessageBroadcaster))]
public class CollidedWithPlanet : MonoBehaviour
{
	public float soundScaleRange = 500;
	public float soundScaleMin = 0.3f;
	public float soundScaleMax = 1;
	
	public float pitchRange = 0.1f;
	
	public float minForce = 50;
	
	void Awake()
	{
		_broadcaster = GetComponent<MessageBroadcaster>();
	}
	
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("Planets"))
		{
			_broadcaster.Broadcast("OnPlanetCollisionEnter", col, SendMessageOptions.DontRequireReceiver);
			
			if(col.relativeVelocity.magnitude > minForce) SFXPlayer.instance.PlayEffect("impact");
		}
	}
	
	void OnCollisionExit(Collision col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("Planets"))
			_broadcaster.Broadcast("OnPlanetCollisionExit", col, SendMessageOptions.DontRequireReceiver);
	}
	
	private MessageBroadcaster _broadcaster;
}

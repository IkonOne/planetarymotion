using UnityEngine;
using System.Collections;

public class PlayerEnergy : MonoBehaviour
{
	public EnergyBarController energyBarController;
	
	public float maxEnergy = 100;
	public float energyDecay = 1;
	public float loopBonus = 10;
	public float comboMult = 2;
	
	void Start()
	{
		_currentEnergy = maxEnergy;
	}
	
	void FixedUpdate()
	{
		AddEnergy(-Time.fixedDeltaTime * energyDecay);
		if(_currentEnergy <= 0) BroadcastMessage("OnEnergyDepleated", this, SendMessageOptions.DontRequireReceiver);
	}
	
	public void AddEnergy(float energy)
	{
		_currentEnergy = Mathf.Clamp(_currentEnergy + energy, 0, maxEnergy);
		energyBarController.SetEnergyPercentage(_currentEnergy / maxEnergy);
	}
	
	void OnPlayerLooped()
	{
		AddEnergy(loopBonus);
	}
	
	private float _currentEnergy;
}

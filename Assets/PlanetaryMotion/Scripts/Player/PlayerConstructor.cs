using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(PauseManager))]
public class PlayerConstructor : MonoBehaviour
{
	public GameObject playerPrefab;
	public GameObject planetLooperPrefab;
	
	public GameObject player;
	public ParticleSystem impactParticles;
	
	public Transform startPosition;
	public Transform impulseTarget;
	public bool planetLooper;
	public bool canPause = true;
	
	void Awake()
	{
		if(startPosition != null) playerPrefab.transform.position = startPosition.transform.position;
		
		GameObject go = (GameObject)Instantiate(playerPrefab);
		go.name = "Player";
		player = go.transform.Find("PlayerPhysics").gameObject;
		impactParticles = go.transform.Find("ImpactParticles").gameObject.particleSystem;
		
		if(canPause)
		{
			PauseManager pm = GetComponent<PauseManager>();
			pm.toPause.Add(player.GetComponent<PauseBehaviour>());
		}
		
		_startMoving = startPosition != null && impulseTarget != null;
		
		if(planetLooper && planetLooperPrefab != null)
		{
			GameObject looper = (GameObject)Instantiate(planetLooperPrefab);
			looper.transform.parent = player.transform;
			looper.GetComponent<PlayerLooped>().player = player;
			looper.GetComponent<MessageBroadcaster>().targets[0] = gameObject;
		}
	}
	
	void FixedUpdate()
	{
		if(_startMoving)
		{
			player.rigidbody.AddForce(impulseTarget.position - startPosition.position, ForceMode.Impulse);
			_startMoving = false;
			enabled = false;
		}
	}
	
	private bool _startMoving;
}

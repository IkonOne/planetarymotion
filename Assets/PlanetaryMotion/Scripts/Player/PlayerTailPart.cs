using UnityEngine;
using System.Collections;

public class PlayerTailPart : MonoBehaviour
{
	public Transform follow;
	public float distance = 20;
	public float springStrength = 3;
	
	// Update is called once per frame
	void Update ()
	{
		transform.position = Vector3.Lerp(transform.position, follow.position, Time.deltaTime * springStrength);
	}
}

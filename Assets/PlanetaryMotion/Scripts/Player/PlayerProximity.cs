using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerConstructor))]
public class PlayerProximity : MonoBehaviour
{
	public EndlessScore playerScore;
	public GameObject player;
	public float maxDist = 1000;
	public float score = 10;
	
	void Awake()
	{
		player = GetComponent<PlayerConstructor>().player;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");
		
		GameObject planet;
		float dist;
		float closestDist = float.MaxValue;
		for (int i = 0; i < planets.Length; i++) {
			planet = planets[i];
			dist = Vector2.Distance(player.transform.position, planet.transform.position) - planet.transform.localScale.x;
			if(dist < closestDist)
			{
				closestDist = dist;
			}
		}
		
		if(closestDist < maxDist)
		{
			playerScore.AddScore((uint)(score * (1 - closestDist / maxDist)));
		}
	}
}

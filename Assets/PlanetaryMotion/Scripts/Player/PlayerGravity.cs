using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class PlayerGravity : MonoBehaviour
{
	public float planetaryMass = 10;
	public float planetaryMultiplier = 0.5f;
	public float focusPlanetaryMass = 100;
	
	public float rigidBodyMaxMass = 1;
	public float rigidBodyMinMass = 0.1f;
	
	public float maxVelocity = 100;
	public float drag = 0.05f;
	
	// Use this for initialization
	void Start ()
	{
		_lineRenderer = GetComponent<LineRenderer>();
	}
	
	void Update()
	{
		if(_selection != null)
		{
			Vector3 endPoint = transform.position;
			endPoint.z = 1;
			_lineRenderer.SetPosition(0, endPoint);
			Vector3 outline = (transform.position - _selection.transform.position).normalized * _selection.GetComponent<Planet>().radius;
			endPoint = _selection.transform.position + outline;
			endPoint.z = 1;
			_lineRenderer.SetPosition(1, endPoint);
			
//			_lineRenderer.SetPosition(0, transform.position);
//			Vector3 outline = (transform.position - _selection.transform.position).normalized * _selection.GetComponent<Planet>().radius;
//			_lineRenderer.SetPosition(1, _selection.transform.position + outline);
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(_selection == null)
		{
			_gravityObjects = GameObject.FindGameObjectsWithTag("Planet");
			foreach (GameObject grav in _gravityObjects)
			{
				if(!grav.activeSelf) continue;
				grav.GetComponent<Planet>().AddGravity(gameObject, planetaryMass);
			}
			rigidbody.mass = rigidBodyMaxMass;
			rigidbody.drag = drag;
			_lineRenderer.enabled = false;
		}
		else
		{
			_selection.GetComponent<Planet>().AddGravity(gameObject, focusPlanetaryMass);
			rigidbody.mass = rigidBodyMinMass;
			rigidbody.drag = 0;
			
			_lineRenderer.enabled = true;
			
		}
		
		if(rigidbody.velocity.sqrMagnitude > maxVelocity * maxVelocity)
			rigidbody.velocity = rigidbody.velocity.normalized * maxVelocity;
	}
	
	void OnPlanetSelected(GameObject selected)
	{
		_selection = selected;
//		SFXPlayer.instance.PlayEffect("grabPlanet");
//		SFXPlayer.instance.PlayEffect("selectPlanet");
	}
	
	void OnPlanetDeselected()
	{
//		SFXPlayer.instance.Stop("grabPlanet");
		_selection = null;
	}
	
	private GameObject[] _gravityObjects;
	private LineRenderer _lineRenderer;
	
	private GameObject _selection;
}

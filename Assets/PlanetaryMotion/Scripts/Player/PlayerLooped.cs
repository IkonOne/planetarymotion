using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer), typeof(MessageBroadcaster))]
public class PlayerLooped : MonoBehaviour
{
	public GameObject player;
	public ParticleSystem particles;
	
	public float energyBonus = 10;
	
	public int linePoints = 20;
	public float lineWidth = 10;
	
	void Start ()
	{
		player = transform.parent.gameObject;
		_lineRenderer = GetComponent<LineRenderer>();
		_lineRenderer.SetWidth(lineWidth, lineWidth);
		_lineRenderer.SetVertexCount(linePoints);
		
		_broadcaster = GetComponent<MessageBroadcaster>();
		
		_totalLoops = 0;
	}
	
	void FixedUpdate ()
	{
		if(_selected == null) return;
		
		if(_lastLooped == null || _selected != _lastLooped.gameObject)
		{
			// Get the current angle
			Vector3 tangent = player.transform.position - _selected.transform.position;
			_currAngle = Mathf.Atan2(tangent.y, tangent.x) * Mathf.Rad2Deg;
			float angleDiff = _currAngle - _prevAngle;
			if(angleDiff > 300) angleDiff -= 360;
			if(angleDiff < -300) angleDiff += 360;
			_totalAngle += angleDiff;
			_prevAngle = _currAngle;
			
			if(_totalAngle > 360 || _totalAngle < -360)
			{
				_lastLooped = _selected.GetComponent<Planet>();
				_totalLoops++;
				RestartLoop();
				
				_broadcaster.Broadcast("OnPlayerLooped", _selected);
				_broadcaster.Broadcast("OnAddEnergy", energyBonus, SendMessageOptions.DontRequireReceiver);
				
				Vector3 particlePos = _selected.transform.position;
				particlePos.z = 2;
				particles.transform.position = particlePos;
				particles.Play();
			}
		}
	}
	
	void LateUpdate()
	{
		_lineRenderer.enabled = _selected != null;
		if(_lineRenderer.enabled)
		{
			CreateCircle();
		}
	}
	
	private void CreateCircle()
	{
		Planet planet = _selected.GetComponent<Planet>();
		_vector3.Set(0, 0, 0);
		
		float angle = _startAngle + 90;
		float angleStep = _totalAngle / (linePoints - 1);
		
		for (int i = 0; i < linePoints; i++)
		{
			_vector3.x = Mathf.Sin(angle * Mathf.Deg2Rad) * (planet.radius + lineWidth / 2 - 1);
			_vector3.y = -Mathf.Cos(angle * Mathf.Deg2Rad) * (planet.radius + lineWidth / 2 - 1);
			_vector3.z = 2;
			_vector3 += planet.transform.position;
			
			_lineRenderer.SetPosition(i, _vector3);
			
			angle += angleStep;
		}
		
	}
				
	private void RestartLoop()
	{
		_totalAngle = 0;
		
		if(_selected != null)
		{
			Vector3 tangent = player.transform.position - _selected.transform.position;
			_startAngle = _prevAngle = Mathf.Atan2(tangent.y, tangent.x) * Mathf.Rad2Deg;
		}
	}
	
	void OnPlayerLooped()
	{
		_vector3 = Vector3.one * 1.1f;
		iTween.PunchScale(_lastLooped.planetGraphics.circle.gameObject, _vector3, 1);
	}
	
	void OnPlanetSelected(GameObject planet)
	{
		_selected = planet;
		RestartLoop();
	}
	
	void OnPlanetDeselected()
	{
		_selected = null;
	}
	
	void OnPlanetCollisionEnter(Collision col)
	{
		if(_selected == col.gameObject) RestartLoop();
	}
	
	private GameObject _selected;
	private Planet _lastLooped;
	private uint _totalLoops;
	
	private float _startAngle;
	private float _currAngle;
	private float _prevAngle;
	private float _totalAngle;
	
	private LineRenderer _lineRenderer;
	private Vector3 _vector3 = new Vector3();
	
	private MessageBroadcaster _broadcaster;
}

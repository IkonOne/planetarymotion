using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerConstructor))]
public class PlayerCircleBounds : MonoBehaviour
{
	public GameObject player;
	public ParticleSystem particles;
	public float maxDistance = 1000;
	public float bounceMult = 0.8f;
	
	void Start()
	{
		player = GetComponent<PlayerConstructor>().player;
		particles = GetComponent<PlayerConstructor>().impactParticles;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(player.transform.position.magnitude > maxDistance)
		{
			player.rigidbody.velocity = Vector3.Reflect(player.rigidbody.velocity, -player.transform.position.normalized) * bounceMult;
			player.transform.position = player.transform.position.normalized * maxDistance;
			SFXPlayer.instance.PlayEffect("impact");
			
			if(particles != null)
			{
				particles.transform.position = player.transform.position;
				particles.transform.LookAt(Vector3.zero);
				particles.Play();
			}
		}
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AwardAnnouncer : MonoBehaviour
{
	public static AwardAnnouncer instance;
	
	public GameObject awardPrefab;
	public int initialCount = 5;
	
	void Awake()
	{
		_activeAwards = new List<GameObject>();
		_inactiveAwards = new List<GameObject>();
		
		GameObject go;
		for (int i = 0; i < initialCount; i++) {
			go = (GameObject)Instantiate(awardPrefab);
			go.transform.parent = transform;
			go.GetComponent<Award>().announcer = this;
			_inactiveAwards.Add(go);
		}
		instance = this;
	}
	
	public void Announce(Vector3 pos, string message, Color color)
	{
		GameObject go = GetAward();
		go.transform.position = pos;
		go.renderer.material.color = Color.white;
		Award award = go.GetComponent<Award>();
		award.color = color;
		award.message = message;
		go.SetActive(true);
	}
	
	public void SetInactive(GameObject award)
	{
		_activeAwards.Remove(award);
		_inactiveAwards.Add(award);
		award.SetActive(false);
	}
	
	public void SetActive(GameObject award)
	{
		_activeAwards.Add(award);
		_inactiveAwards.Remove(award);
	}
	
	private GameObject GetAward()
	{
		GameObject award;
		if(_inactiveAwards.Count > 0)
		{
			award = _inactiveAwards[_inactiveAwards.Count - 1];
			_inactiveAwards.Remove(award);
			return award;
		}
		
		award = (GameObject)Instantiate(awardPrefab);
		award.transform.parent = transform;
		award.GetComponent<Award>().announcer = this;
		_inactiveAwards.Add(award);
		return award;
	}
	
	private List<GameObject> _activeAwards;
	private List<GameObject> _inactiveAwards;
}

﻿using UnityEngine;
using System.Collections;

public class ColorSchemeUICheckBox : ColorSchemeUIButton
{
	public UISprite checkBox;
	
	protected override void Awake ()
	{
		base.Awake ();
		
		if(checkBox != null) checkBox.color = ColorScheme.instance.checkBoxCheck;
	}
}

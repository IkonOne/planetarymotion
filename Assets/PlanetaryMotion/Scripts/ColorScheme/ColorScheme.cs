﻿using UnityEngine;
using System.Collections;

public class ColorScheme : MonoBehaviour
{
	private static ColorScheme _instance;
	public static ColorScheme instance
	{
		get { return _instance; }
	}
	
	public Color buttonBackground;
	public Color buttonHover;
	public Color buttonPressed;
	public Color buttonLabel;
	public Color checkBoxCheck;
	
	public Color MultiplierColor;
	
	void Awake()
	{
		if(_instance != null)
		{
			if(_instance != this) DestroyImmediate(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);
	}
	
	
}

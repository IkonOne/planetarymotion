﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIButton))]
public class ColorSchemeUIButton : MonoBehaviour
{
	public UISprite background;
	public UILabel label;

	// Use this for initialization
	protected virtual void Awake ()
	{
		UIButton button = GetComponent<UIButton>();
		button.hover = ColorScheme.instance.buttonHover;
		button.pressed = ColorScheme.instance.buttonPressed;
		
		if(background != null) background.color = ColorScheme.instance.buttonBackground;
		if(label != null) label.color = ColorScheme.instance.buttonLabel;
	}
}

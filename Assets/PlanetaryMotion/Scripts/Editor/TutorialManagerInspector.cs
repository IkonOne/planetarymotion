﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TutorialManager))]
public class TutorialManagerInspector : Editor
{
	private TutorialManager _manager;
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		_manager = (TutorialManager)target;
		
		List<string> instructions = new List<string>(_manager.instructions);
		
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Instructions.");
		
		for (int i = 0; i < instructions.Count; i++) {
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Toggle(_manager.enablePlanetSelectorAt == i,"Enable Planet Selector:"))
				_manager.enablePlanetSelectorAt = i;
			if (GUILayout.Button("Add")) _add = i;
			if (GUILayout.Button("Del")) _del = i;
			EditorGUILayout.EndHorizontal();
			instructions[i] = EditorGUILayout.TextArea(instructions[i]);
			EditorGUILayout.Space();
		}
		
		if(_add > -1)
		{
			instructions.Insert(_add, "Instruction.");
			if(_add < _manager.enablePlanetSelectorAt)
				_manager.enablePlanetSelectorAt++;
			_add = -1;
		}
		if(_del > -1)
		{
			instructions.RemoveAt(_del);
			if(_del < _manager.enablePlanetSelectorAt)
				_manager.enablePlanetSelectorAt--;
			_del = -1;
		}
		
		((TutorialManager)target).instructions = instructions;
	}
	
	private int _del = -1;
	private int _add = -1;
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GalaxyGenerator : MonoBehaviour
{
	public GameObject planetPrefab;
	
	public int numPlanets = 5;
	
	public float sunMass = 20000;
	public float maxDistance = 1000;
	public float minDistance = 100;
	public float maxApsis = 1000;
	public float planetMassMin = 1000;
	public float planetMasMax = 10000;
	public float planetSpinMin = 10;
	public float planetSpinMax = 100;
	public float sunSpinSpeed = -1;
	public float orbitMinSpeed = 100;
	public float orbitMaxSpeed = 1000;
	
	public Color sunColor;
	public Color[] colors;

	// Use this for initialization
	void Awake () 
	{
		planetPrefab.GetComponent<Planet>().playerScore = GetComponent<EndlessScore>();
		GenerateGalaxy();
	}
	
	void GenerateGalaxy()
	{
		foreach (Transform child in transform)
		{
			Object.Destroy(child.gameObject);
		}
		
		CreateSun();
		for (int i = 0; i < numPlanets; i++) {
			CreatePlanet(_sun.transform, minDistance + (maxDistance / numPlanets) * i);
		}
	}
	
	void CreateSun()
	{
		planetPrefab.transform.position = Vector3.zero;
		planetPrefab.transform.rotation = Quaternion.identity;
		planetPrefab.GetComponent<Planet>().SetMass(sunMass);
		
		_sun = (GameObject)Instantiate(planetPrefab);
		_sun.name = "Sun";
		_sun.transform.parent = transform;
		_sun.rigidbody.constraints |= RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
		
		_sun.GetComponent<Planet>().color = sunColor;
		
		Object.Destroy(_sun.GetComponent<OrbitRenderer>());
		Object.Destroy(_sun.GetComponent<Orbiter>());
	}
	
	void CreatePlanet(Transform orbitAround, float distance)
	{
		planetPrefab.transform.position = Vector3.right * distance;
		planetPrefab.GetComponent<Planet>().SetMass(Random.Range(planetMassMin, planetMasMax));
		
		GameObject instance = (GameObject)Instantiate(planetPrefab);
		instance.name = "Planet";
		
		Orbiter orbiter = instance.AddComponent<Orbiter>();
		orbiter.orbitAround = orbitAround;
		orbiter.apsisDistance = Random.Range(_sun.transform.localScale.x, maxApsis);
		orbiter.startingAngle = Random.value * 360;
		orbiter.orbitSpeed = Random.Range(orbitMinSpeed, orbitMaxSpeed);
		orbiter.circularOrbit = true;
		
		instance.transform.parent = transform;
		instance.GetComponent<Planet>().color = ChooseColor();
	}
	
	Color ChooseColor()
	{
		Color c;
		do {
			c = colors[Random.Range(0, colors.Length)];
		} while (_choosenColors.Contains(c));
		_choosenColors.Add(c);
		if(_choosenColors.Count >= colors.Length) _choosenColors.RemoveAt(0);
		return c;
	}
	
	GameObject CreatePrefab(GameObject prefab)
	{
		GameObject instance = (GameObject)Instantiate(prefab);
		instance.transform.position = Vector3.zero;
		instance.transform.rotation = Quaternion.identity;
		
		return instance;
	}
	
	private GameObject _sun;
	private List<Color> _choosenColors = new List<Color>();
}

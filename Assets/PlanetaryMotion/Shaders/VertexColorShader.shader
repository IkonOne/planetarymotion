Shader "Custom/VertexColorShader"
{
	Properties
	{
		_Shinniness ("Shinniness", Range (0.03, 1)) = 0.03
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		#pragma surface surf BlinnPhong vertex:vert
		half _Shinniness;
		
		struct Input
		{
			half2 uv_MainTex;
			fixed3 vertColors;
		};
		
		void vert (inout appdata_full v, out Input o)
		{
			o.vertColors = v.color.rgb;
//			o.a = v.color.a
			o.uv_MainTex = v.texcoord;
		}
		
		void surf (Input IN, inout SurfaceOutput o)
		{
			o.Albedo = IN.vertColors.rgb;
//			o.a = IN.vertColors.a;
			o.Specular = 1;
			o.Gloss = _Shinniness;
		}
		ENDCG
	} 
	FallBack "Specular"
}

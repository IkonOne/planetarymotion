Shader "Custom/RS3DTexturedAdditive" {
	Properties {
		_GlowColor ("Glow Color", COLOR) = (1,1,1,1)
		_MainTex ("Texture1 (RGB)", 2D) = "white" {}
	}

	Category {
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		Lighting Off
		BindChannels {
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "Texcoord", Texcoord
		}
		
		SubShader {
			Pass {
				ZWrite On
				Cull off
				Blend SrcAlpha One
				SetTexture [_MainTex] {
					Combine texture * primary
				}
			}
		}
	}
}
